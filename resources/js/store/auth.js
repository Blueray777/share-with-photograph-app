import Axios from 'axios';
import { OK, UNPROCESSABLE_ENTITY, CREATED } from '../util';

const state = {
  user: null,
  apiStatus: null,
  loginErrorMessages: null,
  registerErrorMessages: null
};
const getters = {
  check: state => !!state.user,
  username: state => (state.user ? state.user.name : '')
};
const mutations = {
  setUser(state, user) {
    state.user = user;
  },
  setApiStatus(state, status) {
    state.apiStatus = status;
  },
  setLoginErrorMessages(state, messages) {
    state.loginErrorMessages = messages;
  },
  setRegisterErrorMessages(state, messages) {
    state.registerErrorMessages = messages;
  }
};

const actions = {
  // 会員登録
  async register(context, data) {
    // 最初はnull
    context.commit('setApiStatus', null);
    const response = await Axios.post('/api/register', data);
    if (response.status === CREATED) {
      // 成功したらtrue
      context.commit('setApiStatus', true);
      context.commit('setUser', response.data);
      return false;
    }
    // 失敗だったらfalse
    context.commit('setApiStatus', false);
    if (response.status === UNPROCESSABLE_ENTITY) {
      context.commit('setRegisterErrorMessages', response.data.errors);
    } else {
      context.commit('error/setCode', response.status, { root: true });
    }
  },
  // ログイン
  async login(context, data) {
    // 最初はnull
    context.commit('setApiStatus', null);
    const response = await Axios.post('/api/login', data);
    if (response.status === OK) {
      // 成功したらtrue
      context.commit('setApiStatus', true);
      context.commit('setUser', response.data);
      return false;
    }
    // 失敗だったらfalse
    context.commit('setApiStatus', false);
    if (response.status === UNPROCESSABLE_ENTITY) {
      context.commit('setLoginErrorMessages', response.data.errors);
    } else {
      context.commit('error/setCode', response.status, { root: true });
    }
  },
  // ログアウト
  async logout(context) {
    // 最初はnull
    context.commit('setApiStatus', null);
    const response = await Axios.post('/api/logout');
    if (response.status === OK) {
      context.commit('setApiStatus', true);
      context.commit('setUser', null);
      return false;
    }
    context.commit('setApiStatus', false);
    context.commit('error/setCode', response.status, { root: true });
  },
  // ログインユーザーチェック
  async currentUser(context) {
    context.commit('setApiStatus', null);
    const response = await Axios.get('/api/user');
    const user = response.data || null;

    if (response.status === OK) {
      context.commit('setApiStatus', true);
      context.commit('setUser', user);
      return false;
    }
    context.commit('setApiStatus', false);
    context.commit('error/setCode', response.status, { root: true });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
