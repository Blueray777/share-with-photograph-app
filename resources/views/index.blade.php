<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>{{ config('app.name') }}</title>
  <!-- Fonts -->
  <link rel="stylesheet" href="https://use.typekit.net/buh5rof.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/all.css">
  @if(app('env')=='heroku')
  <!-- Styles -->
  <link rel="stylesheet" href="{{ secure_asset('css/style.css') }}">
  @else
  <!-- Styles -->
  <link rel="stylesheet" href="{{ asset('css/style.css') }}">
  @endif
  <!-- Script -->
</head>

<body>
<div id="app"></div>
  @if(app('env')=='heroku')
  <!-- Script -->
  <script src="{{ secure_asset('js/index.js') }}"></script>
  @else
  <!-- Script -->
  <script src="{{ asset('js/index.js') }}"></script>
  @endif
</body>

</html>
