const mix = require('laravel-mix');
require('laravel-mix-polyfill');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

if (!mix.inProduction()) {
  mix.webpackConfig({
    module: {
      rules: [
        {
          enforce: 'pre',
          exclude: /node_modules/,
          loader: 'eslint-loader',
          test: /\.(js|vue)?$/
        },
        // 拡張子.jsのファイルに対する設定
        {
          test: /\.js$/,
          exclude: (file) => /node_modules/.test(file),
          use: [
            {
              loader: 'babel-loader'
            }
          ]
        }
      ]
    }
  });
}

mix
  .js('resources/js/app.js', 'public/js/index.js')
  .sass('resources/sass/app.scss', 'public/css/style.css')
  .polyfill({
    enabled: true,
    useBuiltIns: 'usage',
    targets: { firefox: '50', ie: 11 }
  });
