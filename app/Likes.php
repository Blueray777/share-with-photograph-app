<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Likes extends Model
{

    /**
    * リレーションシップ - usersテーブル
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user(){
        return $this->belongsTo('App\User');
    }

    /**
    * リレーションシップ - picturesテーブル
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function picture(){
        return $this->belongsTo('App\Picture');
    }

    /**
    * リレーションシップ - usersテーブル
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */

    public function getImagesSize(){
        
    }
}
