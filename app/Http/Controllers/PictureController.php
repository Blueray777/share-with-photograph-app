<?php

namespace App\Http\Controllers;
use App\Comment;
use App\Http\Requests\StoreComment;


use App\Http\Requests\StorePicture;
use App\Picture;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\Integer;

class PictureController extends Controller
{
    public function __construct()
    {
        // 認証が必要
        $this->middleware('auth')->except(['index', 'download', 'show']);
    }

    /**
    * 写真一覧
    */
    public function index(){
        $pictures = Picture::with(['owner', 'likes'])
        ->orderBy(Picture::CREATED_AT, 'desc')->paginate();
        //->orderBy(Picture::CREATED_AT, 'desc')->get();
        return $pictures;
    }

    /**
    * 写真詳細
    * @param string $id
    * @return Picture
    */

    public function show(string $id){
        $picture = Picture::where('id', $id)->with(['owner', 'comments.author', 'likes'])->first();
        return $picture ?? abort(404);
    }

    /**
    * 写真投稿
    * @param StorePicture $request
    * @return \Illuminate\Http\Response
    */
    public function create(StorePicture $request){
        // 投稿写真の拡張子を取得する
        $extension = $request->picture->extension();
        $picture = new Picture();

        // インスタンス生成時に割り振られたランダムなID値と
        // 本来の拡張子を組み合わせてファイル名とする
        $picture->filename = $picture->id . '.' . $extension;

        // S3にファイルを保存する
        // 第三引数の'public'はファイルを公開状態で保存するため
        Storage::cloud()->putFileAs('', $request->picture, $picture->filename, 'public');

        // データベースエラー時にファイル削除を行うため
        // トランザクションを利用する
        DB::beginTransaction();

        try{
            Auth::user()->pictures()->save($picture);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            // DBとの不整合を避けるためアップロードしたファイルを削除
            Storage::cloud()->delete($picture->filename);
            throw $exception;
        }

        // リソースの新規作成なので
        // レスポンスコードは201(CREATED)を返却する
        return response($picture, 201);

    }

    /**
    * コメント投稿
    * @param Picture $picture
    * @param StoreComment $request
    * @return \Illuminate\Http\Response
    */

    public function addComment(Picture $picture, StoreComment $request) {
        $comment = new Comment();
        $comment->content = $request->get('content');
        $comment->user_id = Auth::user()->id;
        $picture->comments()->save($comment);

        // authorリレーションをロードするためにコメントを取得しなおす
        $new_comment = Comment::where('id', $comment->id)->with('author')->first();

        return response($new_comment, 201);
    }

    /**
    * いいね解除
    * @param string $id
    * @return array
    */
    public function like(string $id){
        $picture = Picture::where('id', $id)->with('likes')->first();
        
        if(! $picture) {
            abort(404);
        }

        $picture->likes()->detach(Auth::user()->id);
        $picture->likes()->attach(Auth::user()->id);

        return ["picture_id" => $id];
    }


    /**
     * いいね
     * @param string $id
     * @return array
     */
    public function unlike(string $id){
        $picture = Picture::where('id', $id)->with('likes')->first();

        if (!$picture) {
            abort(404);
        }

        $picture->likes()->detach(Auth::user()->id);

        return ["picture_id" => $id];

    }


    /**
    * 写真ダウンロード
    * @param Picture $picture (クラス、インスタンス)
    * @return \Illuminate\Http\Response
    */

    public function download(Picture $picture){
        // 写真の存在チェック
        if (! Storage::cloud()->exists($picture->filename)) {
            abort(404);
        }

        $headers = [
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment; filename="' . $picture->filename . '"',
        ];

        return response(Storage::cloud()->get($picture->filename), 200, $headers);

    }



}
