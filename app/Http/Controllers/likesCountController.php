<?php

namespace App\Http\Controllers;
use App\Likes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Arr;


class likesCountController extends Controller
{
    //
    public function index(){
        $likesValues = [];
        $S3URL = 'https://auspicious.s3.ap-northeast-1.amazonaws.com/';
        // likesテーブルとPicturesテーブルをinner join（内部結合）し
        // likeの数が多い順に10個写真ファイルネームと写真撮影者名と写真IDを取得する
        $likesArray = DB::table('likes')->join('pictures', 'likes.picture_id', '=', 'pictures.id')
                        ->join('users', 'pictures.user_id', '=', 'users.id')
                        ->select(DB::raw('count(likes.picture_id) as `LikesCount`,likes.picture_id, users.name, pictures.filename'))
                        ->groupBy('likes.picture_id')->orderBy('LikesCount', 'desc')->take(5)->get();
        foreach($likesArray as $likeArray){
            // likenのカウント数はいらないので排除
            $collection = collect($likeArray)->except(['LikesCount']);
            $id = $collection['picture_id'];
            $name = $collection['name'];
            $imgUul = $S3URL . $collection['filename'];
            // 画像サイズデータを取得
            $imgData = getimagesize($imgUul);
            // 縦横比率 3:4 よりも横長の画像
            $landscape = $imgData[0] / $imgData[1] >= 0.75;
            // 画像が横向きだったら配列にいれる。
            if($landscape){
                $photoDataArray = ['picture_id'=>$id, 'filename'=>$imgUul, 'name'=>$name];
                array_push($likesValues, $photoDataArray);
            }
        }
        return $likesValues;
    }
}