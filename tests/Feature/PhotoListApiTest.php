<?php

namespace Tests\Feature;

use App\Picture;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PhotoListApiTest extends TestCase
{
    use RefreshDatabase;

    /**
     *
     * @test
     */
    public function should_正しい機能のJSONを返却する()
    {
        $this->withoutExceptionHandling();

        // 5つの写真データを生成する
        factory(Picture::class, 5)->create();
        $response = $this->json('GET', route('picture.index'));

        // 生成した写真データを作成日降順で取得
        $pictures = Picture::with(['owner'])->orderBy('created_at', 'desc')->get();

        // data項目の期待値
        $expected_date = $pictures->map(function($pictures){
          return[
            'id'=>$pictures->id,
            'url'=>$pictures->url,
            'owner'=>[
              'name'=>$pictures->owner->name,
            ],
            'liked_by_user' => false,
          'likes_count' => 0,
          ];
        }) -> all();

        $response->assertStatus(200)
        // レスポンスJSONのdata項目に含まれる要素が5つであること
        ->assertJsonCount(5, 'data')
        // レスポンスJSONのdata項目が期待値と合致すること
        ->assertJsonFragment([
          "data"=>$expected_date,
        ]);
    }
}
