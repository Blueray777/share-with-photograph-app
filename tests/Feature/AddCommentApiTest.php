<?php

namespace Tests\Feature;

use App\Picture;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AddCommentApiTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        // テストユーザー作成
        $this->user = factory(User::class)->create();
    }
    /**
     *
     * @test
     */
    public function should_コメントを追加できる()
    {   
        factory(Picture::class)->create();
        $picture = Picture::first();
        $content = 'sample content';
        $this->withoutExceptionHandling();
 

        $response = $this->actingAs($this->user)
        ->json('POST', route('picture.comment', [
            'picture'=>$picture->id,
        ]), compact('content'));

        $comments = $picture->comments()->get();

        $response->assertStatus(201)
        // JSONフォーマットが期待通りであること
        ->assertJsonFragment([
            "author" => [
                "name" => $this->user->name,
            ],
            "content" => $content,
        ]);

        // DBにコメントが1件登録されていること
        $this->assertEquals(1, $comments->count());
        // 内容がAPIでリクエストしたものであること

        $this->assertEquals($content, $comments[0]->content);

    }
}
