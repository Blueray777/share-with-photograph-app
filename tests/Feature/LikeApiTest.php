<?php

namespace Tests\Feature;

use App\Picture;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LikeApiTest extends TestCase
{
    use RefreshDatabase;
    public function setUp(): void {
        parent::setUp();

        $this->user = factory(User::class) -> create();
        factory(Picture::class)->create();
        $this->picture = Picture::first();
    } 
    /** 
     * @test
     */
    public function should_いいねを追加できる()
    {
        $response = $this->actingAs($this->user)
            ->json('PUT', route('picture.like', [
                'id' => $this->picture->id,
            ]));

        $response->assertStatus(200)
                ->assertJsonFragment([
                    'picture_id' => $this->picture->id,
                ]);
        $this->assertEquals(1, $this->picture->likes()->count());
    }
    /** 
     * @test
     */
    public function should_2回同じ写真にいいねしても1個しかいいねがつかない()
    {
        $param = ['id' => $this->picture->id];
        $this->actingAs($this->user)->json('PUT', route('picture.like', $param));
        $this->actingAs($this->user)->json('PUT', route('picture.like', $param));

        $this->assertEquals(1, $this->picture->likes()->count());

    }
    /** 
     * @test
     */
    public function should_いいねを解除できる()
    {
        $this->picture->likes()->attach($this->user->id);
        $response = $this->actingAs($this->user)
        ->json('DELETE', route('picture.like', [
            'picture'=>$this->picture->id,
        ]));

        $response->assertStatus(200)
        ->assertJsonFragment([
            'picture_id' => $this->picture->id,
        ]);

        $this->assertEquals(0, $this->picture->likes()->count());

    }
}
