<?php

namespace Tests\Feature;

use App\Picture;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LikesCountApiTest extends TestCase
{
    use RefreshDatabase;
    public function setUp(): void {
        parent::setUp();
        $this->user = factory(User::class) -> create();
        factory(Picture::class)->create();
        $this->picture = Picture::first();
    } 

    /** 
     * @test
     */
    public function should_いいねを追加できる()
    {
        $response = $this->actingAs($this->user)
            ->json('PUT', route('picture.like', [
                'id' => $this->picture->id,
            ]));

        $response->assertStatus(200)
                ->assertJsonFragment([
                    'picture_id' => $this->picture->id,
                ]);
        $this->assertEquals(1, $this->picture->likes()->count());
    }

    /** 
     * @test
     */
    public function should_いいねのカウント数を取得()
    {
        $response = $this->json('GET', '/picture', ['likesCount' => '3']);
        $response->assertStatus(200)
        ->assertJson([
            'likesCount'=>true,
        ]);
    }

}
