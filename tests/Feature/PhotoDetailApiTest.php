<?php

namespace Tests\Feature;

use App\Comment;
use App\Picture;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PhotoDetailApiTest extends TestCase
{
    use RefreshDatabase;
    /**
     *
     * @test
     */
    public function should_正しい構造のJSONを返却する()
    {   
        $this->withoutExceptionHandling();

        factory(Picture::class)->create()->each(function ($picture){
            $picture->comments()->saveMany(factory(Comment::class, 3)->make());
        });
        $picture = Picture::first();

        $response = $this->json('GET', route('picture.show', [
            'id' => $picture->id,
        ]));

        $response->assertStatus(200)
        ->assertJsonFragment([
            'id'=>$picture->id,
            'url'=>$picture->url,
            'owner'=>[
                'name'=> $picture->owner->name,
            ],
            'comments'=>$picture->comments
                ->sortByDesc('id')
                ->map(function($comment){
                    return [
                        'author'=>[
                            'name'=>$comment->author->name
                        ],
                        'content'=>$comment->content,
                    ];
                })
            ->all(),
            'liked_by_user' => false,
            'likes_count' => 0,
        ]);
    }
}
