<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// 写真ダウンロード
Route::get('/pictures/{picture}/download', 'PictureController@download');


Route::group(['middleware' => 'auth.very_basic'], function() {
    Route::get('/{any?}', function () {
        return view('index');
    }) -> where('any', '.+');
});