<?php

use App\Http\Controllers\PictureController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// 会員登録
Route::post('/register', 'Auth\RegisterController@register') -> name('register');

// ログイン
Route::post('/login', 'Auth\LoginController@login') -> name('login');

// ログアウト
Route::post('/logout', 'Auth\LoginController@logout') -> name('logout');

// ログインユーザー
Route::get('/user', function(){
  return Auth::user();
})->name('user');
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// トークンリフレッシュ
Route::get('/reflesh-token', function (Illuminate\Http\Request $request) {
  $request->session()->regenerateToken();

  return response()->json();
});

// 写真投稿
Route::post('/pictures', 'PictureController@create')->name('picture.create');

// 写真一覧
Route::get('/pictures', 'PictureController@index')->name('picture.index');

// 写真詳細
Route::get('/pictures/{id}', 'PictureController@show')->name('picture.show');

// コメント
Route::post('/pictures/{picture}/comments', 'PictureController@addComment')->name('picture.comment');

// いいね
Route::put('/pictures/{id}/like', 'PictureController@like')->name('picture.like');

// いいね解除
Route::delete('/pictures/{id}/like', 'PictureController@unlike');

// いいねカウント数
Route::get('/likesCount', 'likesCountController@index');


