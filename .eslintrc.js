// https://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: 'babel-eslint'
  },
  env: {
    node: true,
    es6: true,
    browser: true
  },
  extends: [
    'eslint:recommended',
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    //'plugin:vue/essential',
    // https://github.com/standard/standard/blob/master/docs/RULES-en.md
    //'standard',
    'prettier',
    'prettier/standard',
    //'plugin:vue/recommended',
    'prettier/vue'
  ],
  // required to lint *.vue files
  plugins: ['vue', 'prettier'],
  // add your custom rules here
  rules: {
    // allow async-await
    'generator-star-spacing': 'off',
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    semi: ['error', 'always'],
    'prettier/prettier': [
      'error',
      {
        trailingComma: 'none',
        singleQuote: true,
        semi: true,
        printWidth: 300
      }
    ],
    'vue/v-bind-style': [
      // v-bindを省略しない - e.g)v-bind:foo="bar"
      'error',
      'longform'
    ]
  },
  globals: {
    $: false,
    jQuery: false
  }
};
