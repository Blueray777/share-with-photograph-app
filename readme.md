# 写真共有 APP「Auspicious!」

- [デモサイト](https://alpha-auspicious.herokuapp.com/)

【Basic認証】   
ID : photo  
パスワード : develop  

Laradock & Vue & Vuex & Vue Router を使用して写真共有 APP の作成に TRY する  
お手本にしたのはのチュートリアルシリーズ「[Vue + Vue Router + Vuex + Laravel で写真共有アプリを作ろう(全 16 回)](https://www.hypertextcandy.com/vue-laravel-tutorial-introduction/)」。  
仕様等は「[Vue + Vue Router + Vuex + Laravel で写真共有アプリを作ろう(全 16 回)](https://www.hypertextcandy.com/vue-laravel-tutorial-introduction/)」で定義してあるものを使用

上記と今回と違う点は以下。

- チュートリアルでは Docker の使用はしていないが、今回は Docker を使用したいので Laradoc を採用。
- チュートリアルの DB は PostgreSQL を使用しているが、MySQL の勉強中ということもあり MySQL を使用する。

<dl>
<dt>APP名</dt>
<dd>「Auspicious!」</dd>
</dl>

環境設定は[laradock 複数プロジェクト作成](multi-project.md)を参照。  
環境のディレクトリ構成は以下。

```./LaraDockPRJ
|- laradock
|- share-with-photograph-app
```

git 管理は「share-with-photograph-app」のみ行う。
.env ファイルは git 管理しない。

## 設計書

設計は「[Vue + Vue Router + Vuex + Laravel で写真共有アプリを作ろう(全 16 回)](https://www.hypertextcandy.com/vue-laravel-tutorial-introduction/)」で設計されたものをそのまま使用するが、以下にまとめた。

- [写真共有アプリ設計（Laradock & Vue.js）](https://docs.google.com/spreadsheets/d/1kaWqWCw_D1ZuJtz_aOrmUJip1G4wJjrAVZWm6DyOleE/edit#gid=1595532524)

## 各種環境設定ファイル

この環境に必要な設定変更は以下。

### nginX

`laradock/nginx/sites/app.share_photo_app.conf`

```# server_name app.test;
server_name share-with-photograph-app.test;
# root /var/www/app;
root /var/www/share-with-photograph-app/public;
```

### 注意点

nginX の設定は実際に Laravel を動かすディレクトリ名と同じでないと動かない。

### host

`/private/etc/hosts`

```127.0.0.1 share-with-photograph-app.test
::1 share-with-photograph-app.test
```

変更方法は以下を参考にした。

- [Mac で hosts ファイルを書き換える方法を教えます](https://ischool.co.jp/2018-11-28/)
- [mac で hosts ファイルを書き換える 3 つの方法【保存版】](https://oku-log.com/blog/mac-hosts/)

#### local url

以下、にアクセスして Laravel の文字が出たので成功
[http://share-with-photograph-app.test:8000/](http://share-with-photograph-app.test:8000/)

phpMyAdmin は以下
[http://share-with-photograph-app.test:8080/](http://share-with-photograph-app.test:8080/)

### MySQL

`mysql/docker-entrypoint-initdb.d/createdb.sql`

追記方法は[「laradock 複数プロジェクト作成 - 9.DB を作成」](multi-project.md)を参照

```
CREATE DATABASE IF NOT EXISTS `share-with-photograph-app` COLLATE 'utf8_general_ci' ;
GRANT ALL ON `share-with-photograph-app`.\* TO 'default'@'%' ;
```


### Laravel .env

`/share-with-photograph-app/.env`

```
# DB_CONNECTION=mysql
# DB_HOST=127.0.0.1
DB_HOST=mysql
# DB_PORT=3306
# DB_DATABASE=homestead
DB_DATABASE=share-with-photograph-app
```

docker の MySQL に `mysql -u root -p`でログインした場合のパスワードは  
`laradock/.env` の mysql 設定に記載されているパスワードでログインする。

複数プロジェクトで MySQL を作成した場合、`mysql -u root -p` でログインしないと各プロジェクトの DB を編集できない。

`mysql -u default -p` でログインしない方がよい。

### docker-compose コマンド

### コンテナ起動

`docker-compose up -d nginx mysql`

#### MySQLにログイン

`docker exec -it laradock_mysql_1 /bin/bash`

```
mysql -u root -p[password]
# 「Enter password:」と表示されるので、rootと入力
```

#### laravel work space にユーザーとしてログイン

`docker-compose exec --user=laradock workspace bash`

#### workspace内でwebpackを動かす

workspaceに入る
```
$ docker-compose exec --user=laradock workspace bash
```

workspace内でプロジェクトディレクトリに移動しnpm scriptを実行

e.g)
```
laradock@46ee97a3bfd0:/var/www$ cd project1
laradock@46ee97a3bfd0:/var/www/project1$ npm run dev
```


## テストコードについて

- 記事のLaravel ver 5.7.19
- このリポジトリのLaravel ver 5.8.29

## test 作成コマンド

```
php artisan make:test nameOfTest
```

[Vue + Vue Router + Vuex + Laravelで写真共有アプリを作ろう (4) 認証API](https://www.hypertextcandy.com/vue-laravel-tutorial-authentication/)において、記事が作成された時点と今回使用しているLaravelのversionに相違がありエラーが出る。

気をつける点は以下。

### ログイン API

#### tests/Feature/LoginApiTest.php

##### status code 200 じゃないよエラー

```
Expected status code 200 but received 422.
Failed asserting that false is true.
```

[use --- ] とuse文でインポートする記述の下に「use Illuminate\Http\Request;」を追記する

##### void をつけてねエラー

```
PHP Fatal error:  Declaration of Tests\Feature\LoginApiTest::setUp() must be compatible with Illuminate\Foundation\Testing\TestCase::setUp(): void in /var/www/share-with-photograph-app/tests/Feature/LoginApiTest.php on line 10
```
呼び出し元の「Tests\Feature\LoginApiTest::setUp()」は以下のようになっている。

```
protected function setUp(): void
```

LoginApiTest.php で呼びだす時も以下のように記述する。

```
 public function setUp(): void
    {
        parent::setUp();
        // テストユーザー作成
        $this->user = factory(User::class)->create();
    }
```

##### version upで既存のpasswordが変更になっている

Laravel(v5.7.19)から
`'password' => 'password'` に変更になっているので、記事内の `'password' => 'secret'` を `'password' => 'password'` にする。

#### 写真投稿テストのエラー

#### 参考URL
- [【Laravel】テストを書くときによく使うコード集 - テスト失敗時のメッセージをわかりやすくする](https://public-constructor.com/laravel-test-cheatsheet/#toc13)

以下のエラーメッセージが出たが原因が分かりにくかった。

`expected status code 500 but received 403. `

参考URLを元に`$this->withoutExceptionHandling();`を入れ詳細メッセージが出るようにした。

`Illuminate\Auth\Access\AuthorizationException: This action is unauthorized.`  
とメッセージが出た。

原因は、FormRequestである `app/Http/Requests/StorePicture.php`の`authorize()` がdefaultでfalseになっていたからだった。

[修正前]
```
public function authorize()
    {
        return false;
    }
```

[修正後]
```
public function authorize()
    {
        return true;
    }
```



Illuminate\Auth\Access\AuthorizationException: This action is unauthorized.

## test 実行コマンド

`./vendor/bin/phpunit --testdox`


## マイグレーション

## 実行 コマンド

`php artisan migrate`

## make コマンド

e.g)
```
php artisan make:migration create_pictures_table --create=pictures
php artisan make:migration create_likes_table --create=likes
php artisan make:migration create_comments_table --create=comments
```

### `["SQLSTATE[HY000]: General error: 1215 Cannot add foreign key constraint"]`エラーが出たら

#### 参考URL
- [Laravelのマイグレーションで外部制約する時の注意点](https://qiita.com/haxpig/items/867cde3ec42b24825d32)
- [Laravel 5.8 にて["SQLSTATE[HY000]: General error: 1215 Cannot add foreign key constraint"]エラー](https://qiita.com/ktknj/items/6a3e1936da727dbd92b3)
- [【Laravel】 5.8系にアップデートしてマイグレーションファイルを自動生成する時に注意する事](https://blog.websandbag.com/entry/2019/06/09/111933)

関連させる親tableのカラムの型と小テーブルのカラムの型が同じになるようにする。

e.g)

[Table]
- 親テーブル：users
- 子テーブル：pictures

[関連させたいカラム]
- users Table - id(biginteger)  
- pictures Table - user_id(unsignedBigInteger)

[increments]の場合は[unsignedInteger]

## Model 作成

### コマンドと作成されるのファイルのPath

#### 写真モデル

- コマンド  
`php artisan make:model picture`

- 作成ファイルのパス  
`share-with-photograph-app/app/picture.php`

### コメントモデル

- コマンド
`php artisan make:model Comment`

- 作成ファイルのパス  
`share-with-photograph-app/app/Comment.php`

## リクエストクラス

### 写真フォーム

- コマンド
`php artisan make:request StorePicture`

- 作成ファイルのパス
`share-with-photograph-app/app/Http/Requests/StorePicture.php`

### コメントフォーム

- コマンド
`php artisan make:request StoreComment`

- 作成ファイルのパス
`share-with-photograph-app/app/Http/Requests/StoreComment.php`


## コントローラー

### 写真一覧・詳細の為のコントローラー

- コマンド
`php artisan make:controller PictureController`

- 作成ファイルのパス
`share-with-photograph-app/app/Http/Controllers/PictureController.php`
