# laradock 複数プロジェクト作成

**参考サイト**
- [Laradock で複数の Laravel 開発環境を構築し管理する方法](https://upd.world/laradock-multiple-de/#_Laravel_Laravel)
- [Laradockでの環境構築方法２パターンを細かめに説明](https://qiita.com/wajima/items/69fad6c2b42c52928e82)
- [【Laravel】Laradockで複数プロジェクトを動かす手順](https://pensuke.work/posts/laradock-multiproject/)

## ディレクトリ内の構成を設計
あとでプロジェクトを追加する、最初から2つ作るなど構成を考えディレクトリ構成を設計する。

e.g)
```
./LaraDockPRJ
|- laradock
|- project1
|- project2
```

## Laradock環境開発を作成

### 1.Laradockをclone

下記のコマンドを実行、任意のプロジェクトディレクトリを作成し、作成したディレクトリに移動し、Laradockをclone。

e.g)
```
$ mkdir LaravelProject
$ cd LaravelProject
$ git clone https://github.com/Laradock/laradock.git
```

### 2.[.env]ファイルを作成

laradockディレクトリに移動して「.env」ファイルを作成する。

e.g)
```
$ cd laradock
$ cp env-example .env
```

### 3.Larabel インストール

`docker-compose up`してコンテナ起動

```
$ docker-compose up -d nginx mysql phpmyadmin workspace
```

`docker-compose ps`でコンテナの状況を確認する。

### 4.[.env]ファイルを編集

複数プロジェクトを運用する場合、以下のパス設定は初期のママで良い。

```
APP_CODE_PATH_HOST=../
```

### 5.Laradockにログインする

LaravelをインストールするためにLaradockにユーザーとしてLoginする。

```
$ docker-compose exec --user=laradock workspace bash
```

### 6.composerを利用してLaravelをインストールする
プロジェクトを複数管理するので、必要なプロジェクトごとコマンドを入れる

e.g)
```
laradock@587f31c413cf:/var/www$ composer create-project laravel/laravel project1
laradock@587f31c413cf:/var/www$ composer create-project laravel/laravel project2
```

### 6.ディレクトリ内の構成を確認

ディレクトリ内の構成が当初の設計通りになっていたら、成功。
e.g)
```
./LaraDockPRJ
|- laradock
|- project1
|- project2
```

### 7.各プロジェクトに専用の nginx 設定ファイルを作成して設定

`laradock`ディレクトリに移動。
`laradock/nginx/sites/default.conf`を無効にする

```
cd laradock/
mv laradock/nginx/sites/default.conf default.conf.bk
```

`app.conf.example`をコピーし、各プロジェクトの設定ファイルを作成する。
作成するnginx設定ファイルの名前は、末尾が.confになっていれば何でも構わない。

e.g)
```
$ cp nginx/sites/app.conf.example nginx/sites/app.project1.conf
$ cp nginx/sites/app.conf.example nginx/sites/app.project2.conf
```

.confファイル内の`server_name`と`root`を下記のように変更

e.g)
```
server_name project1.test;
root /var/www/project1/public;
```

変更したら、コンテナを止め再起動する。

```
$ docker-compose stop
$ docker-compose up -d nginx mysql phpmyadmin workspace
```

### 8.hosts ファイルの編集

`http://project1.test`、`http://project2.test`といったURLでアクセスできるようにするために、hostsファイルを編集。

Macの場合は以下に`host`ファイルがある

```
/private/etc/hosts
```
`hosts`ファイルには、下記を追加。

e.g)
```
127.0.0.1 project1.test
::1 project1.test
```

変更方法は以下を参考にした。

- [Macでhostsファイルを書き換える方法を教えます](https://ischool.co.jp/2018-11-28/)
- [macでhostsファイルを書き換える3つの方法【保存版】](https://oku-log.com/blog/mac-hosts/)

#### 注意点
nginXの設定は実際にLaravelを動かすディレクトリ名と同じで無いと動かない。

#### e.g.) local url
以下、にアクセスしてLaravelの文字が出たらOK。
[http://project1.test:8000/](http://project1.test:8000/)


## 9.DBを作成

複数プロジェクトでのmySql設定は以下サイトを参考にした。

- [【Laravel】Laradockで複数プロジェクトを動かす手順](https://pensuke.work/posts/laradock-multiproject/)

`/laradock/mysql/docker-entrypoint-initdb.d/createdb.sql.example` を複製して`createdb.sql`を作成。
DBを作成するsqlを書く。

```
cd mysql/docker-entrypoint-initdb.d/
cp createdb.sql.example createdb.sql
```

`CREATE DATABASE …`と書いてある部分2行を`FLUSH PRIVILEGES ;`の上部にコピペして挿入。
コピペで追記した行のコメントアウトを外す。

`dev_db_1`と書いてある部分をプロジェクト名に変更。

e.g)
```
CREATE DATABASE IF NOT EXISTS `dev_db_1` COLLATE 'utf8_general_ci' ;
GRANT ALL ON `dev_db_1`.* TO 'default'@'%' ;

↓

CREATE DATABASE IF NOT EXISTS `project1` COLLATE 'utf8_general_ci' ;
GRANT ALL ON `project1`.* TO 'default'@'%' ;
```

mySQLコンテナに入る。

```
docker-compose up -d mysql

docker exec -it laradock_mysql_1 /bin/bash
```
rootユーザーでsqlを実行

```
mysql -u root -p < /docker-entrypoint-initdb.d/createdb.sql
# 「Enter password:」と表示されるので、rootと入力
```

DBが作成されたか確認する。

```
mysql -u root -p
show databases;
```

以下のようにDB名があれば成功。

```
+---------------------------+
| Database                  |
+---------------------------+
| information_schema        |
| default                   |
| mysql                     |
| project1                  |　<- ここのように作成されてDB名が表示される
| sys                       |
+---------------------------+
6 rows in set (0.12 sec)
```

## 11.Laravelの.env編集

作成したLaravelプロジェクトの「.env」を編集する。

e.g)
`project1/.env`

DB_HOST、DB_DATABASE、DB_USERNAMEを書き換える。

```
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=project1
DB_USERNAME=default
DB_PASSWORD=secret
```

## 12.workspace内でLaravelのmigrationを行う


workspaceに入る
```
$ docker-compose exec --user=laradock workspace bash
```

workspace内でプロジェクトディレクトリに移動しmigrationを行う。

e.g)
```
laradock@46ee97a3bfd0:/var/www$ cd project1
laradock@46ee97a3bfd0:/var/www/project1$ php artisan migrate
Migration table created successfully.
Migrating: 2014_10_12_000000_create_users_table
Migrated:  2014_10_12_000000_create_users_table (0.19 seconds)
Migrating: 2014_10_12_100000_create_password_resets_table
Migrated:  2014_10_12_100000_create_password_resets_table (0.12 seconds)
```

rootでmysqlにログインしてテーブルができているか確認

e.g)
```
root@13e0d7d630dd:/# mysql -u root -p
Enter password:

mysql> show databases;
mysql> use project1;
mysql> show tables;
+-------------------------------------+
| Tables_in_project1                  |
+-------------------------------------+
| migrations                          |
| password_resets                     |
| users                               |
+-------------------------------------+
3 rows in set (0.01 sec)
```

## 13.workspace内でwebpackを動かす

workspaceに入る
```
$ docker-compose exec --user=laradock workspace bash
```

workspace内でプロジェクトディレクトリに移動しnpm scriptを実行

e.g)
```
laradock@46ee97a3bfd0:/var/www$ cd project1
laradock@46ee97a3bfd0:/var/www/project1$ npm run dev
```








